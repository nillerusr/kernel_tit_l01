<pre>
mt6735, 3.10.65, arm64.

Devices:
Huawei Honor 4c Pro/Huawei Y6 Pro, TIT-L01 (russian/european version)
Huawei Enjoy 5/Huawei Y6 Pro, TIT-AL00 (chinese/belarusian version)

Worked:
LCM
TOUCHSCREEN
WIFI
LEDS
MODEM
etc

Doesn't work:
CAMERA
FLASHLIGHT
etc

Incorrectly works:
ALPS
WAKEUP
SOUND
etc

Unknown:
FM RADIO

Need to edit codegen.dws & add missing drivers.
</pre>
